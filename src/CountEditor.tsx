import { ChangeEventHandler, useCallback, useState } from "react";
import Dialog from "./Dialog";

interface CountEditorProps {
    type?: string;
    onConfirm?: (data: any) => void;
}

export default function CountEditor(props: CountEditorProps) {

    const {
        type,
        onConfirm,
    } = props;

    const defaultState = {
        count: 1,
        dialogOpen: false,
    };

    const [state, setState] = useState(defaultState);

    const countChange = useCallback<ChangeEventHandler<HTMLInputElement>>((e) => {
        const count = Number(e.target.value);
        setState(prev => ({
            ...prev,
            count,
        }));
    }, []);

    const openDialog = useCallback(() => {
        setState(prev => ({
            ...prev,
            dialogOpen: true,
        }));
    }, []);

    const closeDialog = useCallback(() => {
        setState(prev => ({
            ...prev,
            dialogOpen: false,
        }));
    }, []);

    const confirmDialog = useCallback(() => {
        setState(prev => ({
            ...prev,
            dialogOpen: false,
        }));

        if (onConfirm) {
            onConfirm({type, count: state.count});
        }
    }, [type, state.count, onConfirm]);

    return (
        <>
            <button className="btn btn-info" onClick={openDialog}>تأیید</button>
            <Dialog isOpen={state.dialogOpen} onClose={closeDialog}>
                <span>نوع: {type}</span>
                <br />
                <span>تعداد:</span>
                <input type="number" value={state.count} onChange={countChange} />
                <br />
                <button onClick={confirmDialog}>تأیید</button>
            </Dialog>
        </>
    );
}