/* eslint-disable */
import { ChangeEventHandler, useCallback, useState } from "react";
import CountEditor from "./CountEditor";
import "./App.css";

function App() {
    const [type, setType] = useState("");
    const [confirmedData, setConfirmedData] = useState({} as any);

    const typeChange = useCallback<ChangeEventHandler<HTMLInputElement>>((e) => {
        setType(e.target.value);
    }, []);

    const showAlert = useCallback(() => {
        alert(`نوع: ${type}`);
    }, [type]);

    const confirmDialog = useCallback((data: any) => {
        setConfirmedData(data);
    }, []);

    return (
        <div className="app-container">
            <div className="app-content">
                <span className="mb-10">نوع:</span>
                <input className="field-input mb-10" value={type} onChange={typeChange} />

                <div className="action-btns mb-20">
                    <button className="btn btn-primary ml-5" onClick={showAlert}>پیغام</button>
                    <CountEditor type={type} onConfirm={confirmDialog}/>
                </div>

                <div className="output-box field-input mb-10" id="output">
                    خروجی:
                    {confirmedData?.type ? (
                        <>
                        <br />
                        نوع: {confirmedData?.type}
                        <br />
                        تعداد: {confirmedData?.count}
                        </>
                    ) : null}
                </div>
            </div>
        </div>
    );
}

export default App;
